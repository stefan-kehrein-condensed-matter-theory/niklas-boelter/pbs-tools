#!/usr/bin/env python3
import argparse
from collections import defaultdict
import os
import subprocess
import xml.etree.ElementTree as ET


parser = argparse.ArgumentParser(description='Show Running PBS Jobs.',
                                 formatter_class=argparse.RawDescriptionHelpFormatter,
                                 epilog='''Job States
    C -  Job is completed after having run.
    E -  Job is exiting after having run.
    H -  Job is held.
    Q -  job is queued, eligible to run or routed.
    R -  job is running.
    T -  job is being moved to new location.
    W -  job is waiting for its execution time

    Specify --user=all to disable filtering by user.
    ''')
parser.add_argument('-N', '--name', default='',
                    help='Only show jobs with matching names')
parser.add_argument('-U', '--user', default=os.environ.get('USER'),
                    help='Only show jobs for this user')
parser.add_argument('-S', '--state', default='CEHQRTWS',
                    help='Only show jobs with this state(s)')
parser.add_argument('-Q', '--queue', default='all',
                    help='Only show jobs in this queue')
parser.add_argument('-n', '--node', default='',
                    help='Only show jobs running on this node')
parser.add_argument('-l', '--list', action='store_true',
                    help='Only list job ids.')
parser.add_argument('-b', '--batch', action='store_true',
                    help='Output suitable for batch processing.')
parser.add_argument('-c', '--count', action='store_true',
                    help='Only count the number of matching jobs.')
args = parser.parse_args()

if not args.list and not args.batch and not args.count:
    print("Job id State Time Used/Req'd Execution Host    Job Name")

job_owner_count = defaultdict(lambda: 0)
node_owner_count = defaultdict(lambda: 0)
queued_owner_count = defaultdict(lambda: 0)

f = subprocess.Popen('qstat -x', shell=True, stdout=subprocess.PIPE).stdout
dom = ET.parse(f)
job_count = 0
for r in dom.getroot():
    job_owner = r.find("Job_Owner").text
    job_owner = job_owner.split("@")[0]
    job_state = r.find("job_state").text
    job_queue = r.find("queue").text

    if job_state == 'Q' or job_state == 'H':
        queued_owner_count[job_owner] = queued_owner_count[job_owner] + 1

    if job_state == 'R':
        job_owner_count[job_owner] = job_owner_count[job_owner] + 1
        if r.find("exec_host") is not None:
            node_owner_count[job_owner] = node_owner_count[job_owner] + r.find("exec_host").text.count('compute')
    if (job_owner == args.user or args.user == 'all') and (job_queue == args.queue or args.queue == 'all'):
        job_id = r.find("Job_Id").text
        job_id = job_id.split(".")
        job_id = job_id[0]
        job_name = r.find("Job_Name").text
        if args.name != '' and args.name not in job_name:
            continue
        if job_state not in args.state:
            continue
        wtime_used = "--:--:--"
        if r.find("resources_used/walltime") is not None:
            wtime_used = r.find("resources_used/walltime").text
        if not args.batch:
            if job_state == 'Q':
                job_state = "\033[31mQ\033[0m"
            elif job_state == 'R':
                job_state = "\033[32mR\033[0m"
            else:
                job_state = "\033[34m" + job_state + "\033[0m"
        exec_host = 'compute-??-??/??'
        if r.find("exec_host") is not None:
            exec_host = r.find("exec_host").text
        if args.node != '' and args.node not in exec_host:
            continue
        wtime_allowed = r.find("Resource_List/walltime").text
        
        job_count = job_count + 1

        if args.count:
            continue

        if args.list:
            print(job_id)
        elif args.batch:
                print(job_id, job_owner, job_state, wtime_used, wtime_allowed, exec_host, job_name, sep="\t")
        else:
            if args.user == 'all':
                print(job_id, job_owner, job_state, wtime_used, wtime_allowed, exec_host.ljust(17), job_name)
            else:
                print(job_id, job_state, wtime_used, wtime_allowed, exec_host.ljust(17), job_name)
if args.count:
    print(job_count)

if not args.batch and not args.list and not args.count:
    print("\nNumber of queued jobs")
    for job_owner in sorted(queued_owner_count, key=queued_owner_count.get, reverse=True):
        print("\033[32m{:>4}\033[0m".format(queued_owner_count[job_owner]), job_owner)

    print("\nNumber of running jobs")
    for job_owner in sorted(job_owner_count, key=job_owner_count.get, reverse=True):
        print("\033[32m{:>4}\033[0m".format(job_owner_count[job_owner]), job_owner)

    print("\nNumber of cores in usage")
    for job_owner in sorted(node_owner_count, key=node_owner_count.get, reverse=True):
        print("\033[32m{:>4}\033[0m".format(node_owner_count[job_owner]), job_owner)
