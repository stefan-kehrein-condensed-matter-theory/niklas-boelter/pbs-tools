#!/usr/bin/env python3
import argparse
from collections import defaultdict
import subprocess
import xml.etree.ElementTree as ET

f = subprocess.Popen('qstat -x', shell=True, stdout=subprocess.PIPE).stdout

#property_filter = "Gold-"
property_filter = "L2MARCH"

parser = argparse.ArgumentParser(description='Show PBS nodes.')

parser.add_argument('-v', '--verbose', action='store_true',
                            help='Show running jobs as well as nodes')
parser.add_argument('-S', '--state', default='',
                            help='Only show nodes that are UP or DOWN')
args = parser.parse_args()


job_info = {}
dom = ET.parse(f)
for r in dom.getroot():
    job_owner = r.find("Job_Owner").text
    job_owner = job_owner.split("@")[0]
    job_id = r.find("Job_Id").text
    job_name = r.find("Job_Name").text
    queue = r.find("queue").text
    mem = None
    if  r.find("Resource_List/mem") is not None:
        mem = r.find("Resource_List/mem").text
    job_info[job_id] = (queue, mem, job_owner, job_name)
f = subprocess.Popen('qnodes -ax', shell=True, stdout=subprocess.PIPE).stdout

nodes = {}
dom = ET.parse(f)
for r in dom.getroot():
    name = r.find('name').text
    node = {
            "state": r.find('state').text, 
            "processors": r.find('np').text, 
            "properties": '', 
            "nodetype": r.find('ntype').text,
            "gpus": r.find('gpus').text,
            "load": '0.0',
            "memory": "?",
            "message": None,
            "status": None,
            "jobs": None,
            "note": None
        }

    if r.find('properties') is not None:
        node['properties'] = r.find('properties').text

    if property_filter and property_filter not in node['properties']:
        continue

    if r.find('status') is not None:
        node['status'] = r.find('status').text
        for status_param in node['status'].strip().split(","):
            status_param_name, status_param_value =status_param.split("=", 1)
            if status_param_name == 'message':
                node['message'] = status_param_value
            elif status_param_name == 'loadave':
                node['load'] = status_param_value
            elif status_param_name == 'physmem':
                node['memory'] = str(int(status_param_value[:-2])//(1024*1024)) + "gb"
    
    if r.find('jobs') is not None:
        node['jobs'] = r.find('jobs').text
    
    if r.find('note') is not None:
        node['note'] = r.find('note').text

    nodes[name] = node


def keyFunction(x):
    token = x.split("-")
    return int(token[1]) * 1000 + int(token[2])


for nodename in sorted(nodes.keys(), key=keyFunction):
    node = nodes[nodename]
    if node['state'] == "free":
        state = "\033[32m UP \033[0m"
    elif node['state'] == "down" or node['state'] == "down,offline" or node['state'] == "offline":
        state = "\033[31mDOWN\033[0m"
    job_owner_count = {}

    if args.state != '' and args.state not in state:
        continue
    
    print("{:>16}".format(nodename), state, 'load', node['load'] + "/" + node['processors'], 'mem', node['memory'], node['properties'], end = '')
    
    if node['note'] is not None:
        print("\033[31m", node['note'], "\033[0m", end = '')
    if node['message'] is not None:
        print("\033[33m", node['message'], "\033[0m", end = '')

    print()
    
    if state == "\033[31mDOWN\033[0m":
        continue
    f = subprocess.Popen("qnodes {}|grep 'jobs =' ".format(nodename), shell=True, stdout=subprocess.PIPE).stdout
    jobs = f.read().decode('utf-8')
    f.close()
    tokens = jobs.split(",")

    jobs_count = defaultdict(lambda: 0)

    for token in tokens:
        job = token.split("/")
        if len(job) == 2:
            job = job[1].strip()
            if len(job) < 4:
                continue
            jobs_count[job] = jobs_count[job] + 1
    if args.verbose:
        for job in jobs_count.keys():
            count = jobs_count[job]
            count_str = ""
            if count > 1:
                count_str = "{}x".format(count)
            if job in job_info:
                print("     *", count_str, job, *job_info[job])
            else:
                f = subprocess.Popen("qstat {}|tail -n1".format(job), shell=True, stdout=subprocess.PIPE).stdout
                info = f.read()
                f.close()
                print("     *", count_str, info.strip())
